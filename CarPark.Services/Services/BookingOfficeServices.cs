﻿using AutoMapper;
using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using CarPark.Services.IServices;
using CarPark.ViewModels;
using CarPark.ViewModels.BookingOffices;
using System;
using System.Collections.Generic;

namespace CarPark.Services.Services
{
    public class BookingOfficeServices : IBookingOfficeServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BookingOfficeServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<CreateBookingOfficeViewModel> Create(CreateBookingOfficeViewModel bookingOfficeVM)
        {
            try
            {
                var bookingOffice = _mapper.Map<BookingOffice>(bookingOfficeVM);
                _unitOfWork.BookingOfficeRepository.Create(bookingOffice);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateBookingOfficeViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateBookingOfficeViewModel>(ex.Message);
            }
        }

        public ResponseResult<long> Delete(long bookingOfficeId)
        {
            try
            {
                _unitOfWork.BookingOfficeRepository.Delete(bookingOfficeId);
                _unitOfWork.SaveChange();
                return new ResponseResult<long>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<long>(ex.Message);
            }
        }

        public ResponseResult<CreateBookingOfficeViewModel> Edit(CreateBookingOfficeViewModel bookingOfficeVM)
        {
            try
            {
                var bookingOffice = _mapper.Map<BookingOffice>(bookingOfficeVM);
                
                _unitOfWork.BookingOfficeRepository.Update(bookingOffice);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateBookingOfficeViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateBookingOfficeViewModel>(ex.Message);
            }
        }

        public IEnumerable<BookingOfficeViewModel> Filter(string name, string phone, string place)
        {
            try
            {
                var bookingOffices = _unitOfWork.BookingOfficeRepository.Filter(name, phone, place);  
                return _mapper.Map<IEnumerable<BookingOfficeViewModel>>(bookingOffices);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<BookingOfficeViewModel> GetAll()
        {
            try
            {
                var bookingOffices = _unitOfWork.BookingOfficeRepository.GetDetailAll();
                return _mapper.Map<IEnumerable<BookingOfficeViewModel>>(bookingOffices);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public BookingOfficeDetailViewModel GetById(long id)
        {
            try
            {
                var bookingOffice = _unitOfWork.BookingOfficeRepository.GetDetailById(id);
                return _mapper.Map<BookingOfficeDetailViewModel>(bookingOffice);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
