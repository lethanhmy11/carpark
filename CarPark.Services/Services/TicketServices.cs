﻿using AutoMapper;
using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using CarPark.Services.IServices;
using CarPark.ViewModels;
using CarPark.ViewModels.Tickets;
using System;
using System.Collections.Generic;


namespace CarPark.Services.Services
{
    public class TicketServices : ITicketServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TicketServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<CreateTicketViewModel> Create(CreateTicketViewModel ticketVM)
        {
            try
            {
                var ticket = _mapper.Map<Ticket>(ticketVM);
                _unitOfWork.TicketRepository.Create(ticket);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateTicketViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateTicketViewModel>(ex.Message);
            }
        }

        public ResponseResult<long> Delete(long ticketId)
        {
            try
            {
                _unitOfWork.TicketRepository.Delete(ticketId);
                _unitOfWork.SaveChange();
                return new ResponseResult<long>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<long>(ex.Message);
            }
        }

        public ResponseResult<CreateTicketViewModel> Edit(CreateTicketViewModel ticketVM)
        {
            try
            {
                var ticket = _mapper.Map<Ticket>(ticketVM);
                _unitOfWork.TicketRepository.Update(ticket);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateTicketViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateTicketViewModel>(ex.Message);
            }
        }

        public IEnumerable<TicketViewModel> Filter(string licensePlate, string destination, string customer, DateTime departureDate)
        {
            try
            {
                var tickets = _unitOfWork.TicketRepository.Filter(licensePlate, destination, customer, departureDate);
                return _mapper.Map<IEnumerable<TicketViewModel>>(tickets);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<TicketViewModel> GetAll()
        {
            try
            {
                var tickets = _unitOfWork.TicketRepository.GetDetailAll();
                return _mapper.Map<IEnumerable<TicketViewModel>>(tickets);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public TicketViewModel GetById(long id)
        {
            try
            {
                var ticket = _unitOfWork.TicketRepository.GetDetailById(id);
                return _mapper.Map<TicketViewModel>(ticket);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
