﻿using AutoMapper;
using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using CarPark.Services.IServices;
using CarPark.ViewModels;
using CarPark.ViewModels.Trips;
using System;
using System.Collections.Generic;

namespace CarPark.Services.Services
{
    public class TripServices : ITripServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TripServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<CreateTripViewModel> Create(CreateTripViewModel tripVM)
        {
            try
            {
                var trip = _mapper.Map<Trip>(tripVM);
                _unitOfWork.TripRepository.Create(trip);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateTripViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateTripViewModel>(ex.Message);
            }
        }

        public ResponseResult<long> Delete(long tripId)
        {
            try
            {
                _unitOfWork.TripRepository.Delete(tripId);
                _unitOfWork.SaveChange();
                return new ResponseResult<long>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<long>(ex.Message);
            }
        }

        public ResponseResult<CreateTripViewModel> Edit(CreateTripViewModel tripVM)
        {
            try
            {
                var trip = _mapper.Map<Trip>(tripVM);
                _unitOfWork.TripRepository.Update(trip);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateTripViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateTripViewModel>(ex.Message);
            }
        }

        public IEnumerable<TripViewModel> Filter(DateTime departureDate)
        {
            try
            {
                var trips = _unitOfWork.TripRepository.Filter(departureDate);
                return _mapper.Map<IEnumerable<TripViewModel>>(trips);   
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<TripViewModel> GetAll()
        {
            try
            {
                var trips = _unitOfWork.TripRepository.GetAll();
                return _mapper.Map<IEnumerable<TripViewModel>>(trips);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Trip GetById(long id)
        {
            try
            {
                return _unitOfWork.TripRepository.GetById(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
