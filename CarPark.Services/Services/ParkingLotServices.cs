﻿using AutoMapper;
using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using CarPark.Services.IServices;
using CarPark.ViewModels;
using CarPark.ViewModels.ParkingLots;
using System;
using System.Collections.Generic;


namespace CarPark.Services.Services
{
    public class ParkingLotServices : IParkingLotServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ParkingLotServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<CreateParkingLotViewModel> Create(CreateParkingLotViewModel parkingLotVM)
        {
            try
            {
                var parkingLot = _mapper.Map<ParkingLot>(parkingLotVM);
                _unitOfWork.ParkingLotRepository.Create(parkingLot);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateParkingLotViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateParkingLotViewModel>(ex.Message);
            }
        }

        public ResponseResult<long> Delete(long parkingLotId)
        {
            try
            {
                _unitOfWork.ParkingLotRepository.Delete(parkingLotId);
                _unitOfWork.SaveChange();
                return new ResponseResult<long>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<long>(ex.Message);
            }
        }

        public ResponseResult<CreateParkingLotViewModel> Edit(CreateParkingLotViewModel parkingLotVM)
        {
            try
            {
                var parkingLot = _mapper.Map<ParkingLot>(parkingLotVM);
                _unitOfWork.ParkingLotRepository.Update(parkingLot);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateParkingLotViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateParkingLotViewModel>(ex.Message);
            }
        }

        public IEnumerable<ParkingLot> Filter(string name, string place)
        {
            try
            {
                var parkingLots = _unitOfWork.ParkingLotRepository.Filter(name, place);
                return _mapper.Map<IEnumerable<ParkingLot>>(parkingLots);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<ParkingLot> GetAll()
        {
            try
            {
                var parkingLots = _unitOfWork.ParkingLotRepository.GetAll();
                return _mapper.Map<IEnumerable<ParkingLot>>(parkingLots);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ParkingLot GetById(long id)
        {
            try
            {
                var parkingLot = _unitOfWork.ParkingLotRepository.GetById(id);
                return _mapper.Map<ParkingLot>(parkingLot);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
