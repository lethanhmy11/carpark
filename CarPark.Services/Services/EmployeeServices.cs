﻿using AutoMapper;
using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using CarPark.Helper;
using CarPark.Services.IServices;
using CarPark.ViewModels;
using CarPark.ViewModels.Employees;
using System;
using System.Collections.Generic;

namespace CarPark.Services.Services
{
    public class EmployeeServices : IEmployeeServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public EmployeeServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Employee Authenticate(string account, string password)
        {
            try
            {
                var employee = _unitOfWork.EmployeeRepository.Authenticate(account, password);
                return employee;
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<Employee> Create(Employee employee)
        {
            try
            {
                if(!employee.Password.PasswordValidation())
                {
                    return new ResponseResult<Employee>("The password must have at least 6 character," +
                        " including uppercase, lowercase and number");
                }
                _unitOfWork.EmployeeRepository.Create(employee);
                _unitOfWork.SaveChange();
                return new ResponseResult<Employee>();

            }catch(Exception ex)
            {
                return new ResponseResult<Employee>(ex.Message);
            }
        }

        public ResponseResult<long> Delete(long employeeId)
        {
            try
            {
                _unitOfWork.EmployeeRepository.Delete(employeeId);
                _unitOfWork.SaveChange();
                return new ResponseResult<long>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<long>(ex.Message);
            }
        }

        public ResponseResult<Employee> Edit(Employee employee)
        {
            try
            {
                if (!employee.Password.PasswordValidation())
                {
                    return new ResponseResult<Employee>("The password must have at least 6 character," +
                        " including uppercase, lowercase and number");
                }
                _unitOfWork.EmployeeRepository.Update(employee);
                _unitOfWork.SaveChange();
                return new ResponseResult<Employee>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<Employee>(ex.Message);
            }
        }

        public IEnumerable<EmployeeViewModel> Filter(string name, string department, string sex)
        {
            try
            {
                var employees = _unitOfWork.EmployeeRepository.Filter(name, department, sex);
                return _mapper.Map<IEnumerable<EmployeeViewModel>>(employees);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<EmployeeViewModel> GetAll()
        {
            try
            {
                var employees = _unitOfWork.EmployeeRepository.GetAll();
                return _mapper.Map<IEnumerable<EmployeeViewModel>>(employees);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Employee GetById(long id)
        {
            try
            {
                return _unitOfWork.EmployeeRepository.GetById(id);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
