﻿using AutoMapper;
using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using CarPark.Services.IServices;
using CarPark.ViewModels;
using CarPark.ViewModels.Cars;
using System;
using System.Collections.Generic;

namespace CarPark.Services.Services
{
    public class CarServices : ICarServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CarServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<CreateCarViewModel> Create(CreateCarViewModel carVM)
        {
            try
            {
                var car = _mapper.Map<Car>(carVM);  
                _unitOfWork.CarRepository.Create(car);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateCarViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateCarViewModel>(ex.Message);
            }
        }

        public ResponseResult<string> Delete(string licensePlate)
        {
            try
            {
                _unitOfWork.CarRepository.Delete(licensePlate);
                _unitOfWork.SaveChange();
                return new ResponseResult<string>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public ResponseResult<CreateCarViewModel> Edit(CreateCarViewModel carVM)
        {
            try
            {
                var car = _mapper.Map<Car>(carVM);
                _unitOfWork.CarRepository.Update(car);
                _unitOfWork.SaveChange();
                return new ResponseResult<CreateCarViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateCarViewModel>(ex.Message);
            }
        }

        public IEnumerable<CarViewModel> Filter(string licensePlate, string carType, string company)
        {
            try
            {
                var cars = _unitOfWork.CarRepository.Filter(licensePlate, carType, company);
                return _mapper.Map<IEnumerable<CarViewModel>>(cars);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<CarViewModel> GetAll()
        {
            try
            {
                var cars = _unitOfWork.CarRepository.GetDetailAll();
                return _mapper.Map<IEnumerable<CarViewModel>>(cars);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Car GetById(string licensePlate)
        {
            try
            {
                var car = _unitOfWork.CarRepository.GetDetailById(licensePlate);
                return _mapper.Map<Car>(car);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
