﻿using CarPark.Core.Models;
using CarPark.ViewModels;
using CarPark.ViewModels.Tickets;
using System;
using System.Collections.Generic;


namespace CarPark.Services.IServices
{
    public interface ITicketServices
    {
        TicketViewModel GetById(long id);
        IEnumerable<TicketViewModel> GetAll();
        IEnumerable<TicketViewModel> Filter(string licensePlate, string destination, string customer, DateTime departureDate);
        ResponseResult<CreateTicketViewModel> Create(CreateTicketViewModel ticketVM);
        ResponseResult<CreateTicketViewModel> Edit(CreateTicketViewModel ticketVM);
        ResponseResult<long> Delete(long ticketId);
    }
}
