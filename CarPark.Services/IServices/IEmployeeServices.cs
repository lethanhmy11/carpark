﻿using CarPark.Core.Models;
using CarPark.ViewModels;
using CarPark.ViewModels.Employees;
using System.Collections.Generic;


namespace CarPark.Services.IServices
{
    public interface IEmployeeServices
    {
        Employee GetById(long id);
        IEnumerable<EmployeeViewModel> GetAll();
        IEnumerable<EmployeeViewModel> Filter(string name, string department, string sex);
        ResponseResult<Employee> Create(Employee employee);
        ResponseResult<Employee> Edit(Employee employee);
        ResponseResult<long> Delete(long employeeId);
        Employee Authenticate(string account, string password);
    }
}
