﻿using CarPark.Core.Models;
using CarPark.ViewModels;
using CarPark.ViewModels.Trips;
using System;
using System.Collections.Generic;


namespace CarPark.Services.IServices
{
    public interface ITripServices
    {
        Trip GetById(long id);
        IEnumerable<TripViewModel> GetAll();
        IEnumerable<TripViewModel> Filter(DateTime departureDate);
        ResponseResult<CreateTripViewModel> Create(CreateTripViewModel tripVM);
        ResponseResult<CreateTripViewModel> Edit(CreateTripViewModel tripVM);
        ResponseResult<long> Delete(long tripId); 
    }
}
