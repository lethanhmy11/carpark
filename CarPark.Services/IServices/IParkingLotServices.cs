﻿using CarPark.Core.Models;
using CarPark.ViewModels;
using CarPark.ViewModels.ParkingLots;
using System.Collections.Generic;

namespace CarPark.Services.IServices
{
    public interface IParkingLotServices
    {
        ParkingLot GetById(long id);
        IEnumerable<ParkingLot> GetAll();
        IEnumerable<ParkingLot> Filter(string name, string place);
        ResponseResult<CreateParkingLotViewModel> Create(CreateParkingLotViewModel parkingLotVM);
        ResponseResult<CreateParkingLotViewModel> Edit(CreateParkingLotViewModel parkingLot);
        ResponseResult<long> Delete(long parkingLotId);
    }
}
