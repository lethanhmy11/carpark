﻿using CarPark.Core.Models;
using CarPark.ViewModels;
using CarPark.ViewModels.Cars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Services.IServices
{
    public interface ICarServices
    {
        Car GetById(string licensePlate);
        IEnumerable<CarViewModel> GetAll();
        IEnumerable<CarViewModel> Filter(string licensePlate, string carType, string company);
        ResponseResult<CreateCarViewModel> Create(CreateCarViewModel carVM);
        ResponseResult<CreateCarViewModel> Edit(CreateCarViewModel carVM);
        ResponseResult<string> Delete(string licensePlate);
    }
}
