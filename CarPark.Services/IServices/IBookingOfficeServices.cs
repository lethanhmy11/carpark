﻿using CarPark.Core.Models;
using CarPark.ViewModels;
using CarPark.ViewModels.BookingOffices;
using System.Collections.Generic;


namespace CarPark.Services.IServices
{
    public interface IBookingOfficeServices
    {
        BookingOfficeDetailViewModel GetById(long id);
        IEnumerable<BookingOfficeViewModel> GetAll();
        IEnumerable<BookingOfficeViewModel> Filter(string name, string phone, string place);
        ResponseResult<CreateBookingOfficeViewModel> Create(CreateBookingOfficeViewModel bookingOfficeVM);
        ResponseResult<CreateBookingOfficeViewModel> Edit(CreateBookingOfficeViewModel bookingOfficeVM);
        ResponseResult<long> Delete(long bookingOfficeId);
    }
}
