﻿using CarPark.Core.Infrastructures;
using CarPark.Core.IRepositories;
using CarPark.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarPark.Core.Repositories
{
    public class TripRepository : GenericRepository<Trip>, ITripRepository
    {
        private readonly CarParkDBContext _context;

        public TripRepository(CarParkDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Trip> Filter(DateTime departureDate)
        {
            DateTime defautDate = new DateTime(0001, 01, 01);
            if (departureDate == null || departureDate == defautDate)
                return _context.Trips;
            return _context.Trips.Where(x => x.DepartureDate.Date == departureDate.Date);           
        }
    }
}
