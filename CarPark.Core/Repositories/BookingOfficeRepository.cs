﻿using CarPark.Core.Infrastructures;
using CarPark.Core.IRepositories;
using CarPark.Core.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CarPark.Core.Repositories
{
    public class BookingOfficeRepository : GenericRepository<BookingOffice>, IBookingOfficeRepository
    {
        private readonly CarParkDBContext _context;

        public BookingOfficeRepository(CarParkDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<BookingOffice> Filter(string name, string phone, string place)
        {
            var bookingOffices = _context.BookingOffices.Include(x => x.Trip).AsQueryable();
            if(name != null)
            {
                bookingOffices = bookingOffices.Where(x => x.OfficeName.ToUpper().Contains(name.ToUpper()));
            }

            if(phone != null)
            {
                bookingOffices = bookingOffices.Where(x => x.OfficePhone.Contains(phone));
            }

            if(place != null)
            {
                bookingOffices = bookingOffices.Where(x=> x.OfficePlace.ToUpper().Contains(place.ToUpper()));
            }
            return bookingOffices;
        }

        public IEnumerable<BookingOffice> GetDetailAll()
        {
            return _context.BookingOffices.Include(x => x.Trip);
        }

        public BookingOffice GetDetailById(long id)
        {
            return _context.BookingOffices.Include(x => x.Trip).Where(x => x.OfficeId == id).FirstOrDefault();
        }
    }
}
