﻿using CarPark.Core.Infrastructures;
using CarPark.Core.IRepositories;
using CarPark.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarPark.Core.Repositories
{
    public class CarRepository : GenericRepository<Car>, ICarRepository
    {
        private readonly CarParkDBContext _context;

        public CarRepository(CarParkDBContext context) : base(context)
        {
            _context = context;
        }

        public void Delete(string licensePlate)
        {
            Car car = _context.Cars.Where(x => x.LicensePlate == licensePlate).FirstOrDefault();
            if (car != null)
                _context.Remove(car);
            else
                throw new ArgumentNullException($"Car has lincense plate = {licensePlate} not exist !");
        }

        public IEnumerable<Car> Filter(string licensePlate, string carType, string company)
        {
            var cars = _context.Cars.Include(x => x.ParkingLot).AsQueryable();
            if(licensePlate != null)
            {
                cars = cars.Where(x => x.LicensePlate.ToLower().Contains(licensePlate.ToLower()));
            }
            if(carType != null)
            {
                cars = cars.Where(x => x.CarType.ToLower().Contains(carType.ToLower()));
            }
            if(company != null)
            {
                cars = cars.Where( x=> x.Company.ToLower().Contains(company.ToLower()));
            }
            return cars;
        }

        public IEnumerable<Car> GetDetailAll()
        {
            var ttt= _context.Cars.Include(x => x.ParkingLot).ToList();
            return _context.Cars.Include(x => x.ParkingLot);
        }

        public Car GetDetailById(string licensePlate)
        {
            return _context.Cars.Include(x => x.ParkingLot).Where(x => x.LicensePlate == licensePlate).FirstOrDefault();
        }
    }
}
