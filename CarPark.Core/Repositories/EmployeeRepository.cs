﻿using CarPark.Core.Infrastructures;
using CarPark.Core.IRepositories;
using CarPark.Core.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace CarPark.Core.Repositories
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        private readonly CarParkDBContext _context;
        private readonly AppSettings _appSetting;

        public EmployeeRepository(CarParkDBContext context) : base(context)
        {
            _context = context;
        }

        public EmployeeRepository(CarParkDBContext context, IOptions<AppSettings> appSetting) : base(context)
        {
            _context = context;
            _appSetting = appSetting.Value;
        }

        public Employee Authenticate(string account, string password)
        {
            var employee = _context.Employees.SingleOrDefault(x => x.Account == account && x.Password == password);
            if (employee == null)
            {
                return null;
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSetting.Secret);
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name,employee.EmployeeId.ToString()),
                    new Claim(ClaimTypes.Role, employee.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescription);
            employee.Token = tokenHandler.WriteToken(token);
            employee.Password = "";
            return employee;
        }

        public IEnumerable<Employee> Filter(string name, string department, string sex)
        {
            var employees = _context.Employees.AsQueryable();
            if(name != null)
            {
                employees = employees.Where(x => x.EmployeeName.ToUpper().Contains(name.ToUpper()));
            }
            if(department != null)
            {
                employees = employees.Where(x=>x.Department.ToUpper().Contains(department.ToUpper()));
            }
            if(sex != null)
            {
                employees = employees.Where(x => x.Sex.Equals(sex));
            }
            return employees;
        }
    }
}
