﻿using CarPark.Core.Infrastructures;
using CarPark.Core.IRepositories;
using CarPark.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarPark.Core.Repositories
{
    public class TicketRepository : GenericRepository<Ticket>, ITicketRepository
    {
        private readonly CarParkDBContext _context;

        public TicketRepository(CarParkDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Ticket> Filter(string licensePlate, string destination, string customer, DateTime departureDate)
        {
            var tickets = _context.Tickets.Include(x => x.Trip).AsQueryable();
            if(licensePlate != null)
            {
                tickets = tickets.Where(x => x.LicensePlate.ToLower().Contains(licensePlate.ToLower()));
            }
            if(destination != null)
            {
                tickets = tickets.Where(x => x.Trip.Destination.ToLower().Contains(destination.ToLower()));
            }
            if(customer != null)
            {
                tickets = tickets.Where(x => x.CustomerName.ToLower().Contains(customer.ToLower()));
            }
            DateTime defaultDate = new DateTime(0001, 01, 01);
            if (departureDate != defaultDate && departureDate != null)
            {
                tickets = tickets.Where(x => x.Trip.DepartureDate.Date == departureDate.Date);
            }
            return tickets;
        }

        public IEnumerable<Ticket> GetDetailAll()
        {
            return _context.Tickets.Include(x => x.Trip);
        }

        public Ticket GetDetailById(long id)
        {
            return _context.Tickets.Include(x => x.Trip).Where(x => x.TicketId == id).FirstOrDefault();
        }
    }
}
