﻿using CarPark.Core.Infrastructures;
using CarPark.Core.IRepositories;
using CarPark.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace CarPark.Core.Repositories
{
    public class ParkingLotRepository : GenericRepository<ParkingLot>, IParkingLotRepository
    {
        private readonly CarParkDBContext _context;

        public ParkingLotRepository(CarParkDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<ParkingLot> Filter(string name, string place)
        {
            var parkingLots = _context.ParkingLots.AsQueryable();
            if(name != null)
            {
                parkingLots = parkingLots.Where(x => x.ParkName.ToUpper().Contains(name.ToUpper()));
            }
            if(place != null)
            {
                parkingLots = parkingLots.Where(x => x.ParkPlace.ToUpper().Contains(place.ToUpper()));
            }

            return parkingLots;
        }
    }
}
