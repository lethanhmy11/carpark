﻿using System;
using System.ComponentModel.DataAnnotations;


namespace CarPark.Core.Models
{
    public class BookingOffice
    {
        [Key]
        public long OfficeId { get; set; }

        public DateTime EndContractDeadline { get; set; }

        public string OfficeName { get; set; }

        public string OfficePhone { get; set; }

        public string OfficePlace { get; set; }

        public long OfficePrice { get; set; }

        public DateTime StartContractDeadline { get; set; }

        public long TripId { get; set; }

        public Trip Trip { get; set; }
    }
}
