﻿using System;
using System.ComponentModel.DataAnnotations;


namespace CarPark.Core.Models
{
    public class Ticket
    {
        [Key]
        public long TicketId { get; set; }

        public TimeSpan BookingTime { get; set; }

        public string CustomerName { get; set; }

        public string LicensePlate { get; set; }   

        public long TripId { get; set; }

        public Car Car { set; get; }

        public Trip Trip { set; get; }
    }
}
