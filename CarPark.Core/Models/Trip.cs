﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarPark.Core.Models
{
    public class Trip
    {
        [Key]
        public long TripId { get; set; }

        public long BookedTickedNumber{ get; set; }

        public string CarType { get; set; }

        public DateTime DepartureDate { get; set; }

        public TimeSpan DepartureTime { get; set; }

        public string Destination { get; set; }

        public string Driver { get; set; }

        public long MaximumOnlineNumber { get; set; }

        public ICollection<Ticket> Tickets { get; set; }

        public ICollection<BookingOffice> BookingOffices { get; set; }
    }
}
