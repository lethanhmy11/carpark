﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Core.Models
{
    public class Car
    {
        [Key]
        public string LicensePlate { get; set; }

        public string CarColor { get; set; }

        public string CarType { get; set; }

        public string Company { get; set; }
        
        public long ParkId { get; set; }

        [ForeignKey("ParkId")]
        public ParkingLot ParkingLot { get; set; }

        public ICollection<Ticket> Tickets {get; set; }
    }
}
