﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Core.Models
{
    public class ParkingLot
    {
        [Key]
        public long ParkId { get; set; }

        public long ParkArea { get; set; }

        public string ParkName { get; set; }

        public string ParkPlace { get; set; }

        public long ParkPrice { get; set; }

        public string ParkStatus { get; set; }

        public ICollection<Car> Cars { get; set; }

       
    }
}
