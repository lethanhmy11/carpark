﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarPark.Core.Models
{
    public class Employee
    {
        [Key]  
        public long EmployeeId { get; set; }

        public string Account { get; set; }

        public string Department { get; set; }

        public string  EmployeeAddress { get; set; }

        public DateTime EmployeeBirthDate { get; set; }

        public string EmployeeEmail { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeePhone { get; set; }

        public string Password { get; set; }

        public string Sex { get; set; }

        public string Role { get; set; }

        [NotMapped]
        public string Token { get; set; }
    }
}
