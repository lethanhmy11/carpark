﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CarPark.Core.Migrations
{
    public partial class AddColumnRoleInTableEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Role",
                table: "Employees",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "BookingOffices",
                keyColumn: "OfficeId",
                keyValue: 3L,
                column: "OfficePhone",
                value: "0236514777");

            migrationBuilder.UpdateData(
                table: "Trips",
                keyColumn: "TripId",
                keyValue: 1L,
                column: "DepartureDate",
                value: new DateTime(2022, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "Employees");

            migrationBuilder.UpdateData(
                table: "BookingOffices",
                keyColumn: "OfficeId",
                keyValue: 3L,
                column: "OfficePhone",
                value: "0236514578");

            migrationBuilder.UpdateData(
                table: "Trips",
                keyColumn: "TripId",
                keyValue: 1L,
                column: "DepartureDate",
                value: new DateTime(2022, 4, 21, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
