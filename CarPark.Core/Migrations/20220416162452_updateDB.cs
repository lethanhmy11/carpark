﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarPark.Core.Migrations
{
    public partial class updateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cars_ParkingLots_ParkingLotParkId",
                table: "Cars");

            migrationBuilder.DropIndex(
                name: "IX_Cars_ParkingLotParkId",
                table: "Cars");

            migrationBuilder.DropColumn(
                name: "ParkingLotParkId",
                table: "Cars");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ParkId",
                table: "Cars",
                column: "ParkId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cars_ParkingLots_ParkId",
                table: "Cars",
                column: "ParkId",
                principalTable: "ParkingLots",
                principalColumn: "ParkId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cars_ParkingLots_ParkId",
                table: "Cars");

            migrationBuilder.DropIndex(
                name: "IX_Cars_ParkId",
                table: "Cars");

            migrationBuilder.AddColumn<long>(
                name: "ParkingLotParkId",
                table: "Cars",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ParkingLotParkId",
                table: "Cars",
                column: "ParkingLotParkId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cars_ParkingLots_ParkingLotParkId",
                table: "Cars",
                column: "ParkingLotParkId",
                principalTable: "ParkingLots",
                principalColumn: "ParkId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
