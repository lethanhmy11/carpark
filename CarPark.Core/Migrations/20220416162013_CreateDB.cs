﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CarPark.Core.Migrations
{
    public partial class CreateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Account = table.Column<string>(type: "varchar(50)", nullable: true),
                    Department = table.Column<string>(type: "varchar(50)", nullable: true),
                    EmployeeAddress = table.Column<string>(type: "varchar(50)", nullable: true),
                    EmployeeBirthDate = table.Column<DateTime>(type: "date", nullable: false),
                    EmployeeEmail = table.Column<string>(type: "varchar(50)", nullable: true),
                    EmployeeName = table.Column<string>(type: "varchar(50)", nullable: true),
                    EmployeePhone = table.Column<string>(type: "varchar(10)", nullable: true),
                    Password = table.Column<string>(type: "varchar(20)", nullable: true),
                    Sex = table.Column<string>(type: "varchar(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                });

            migrationBuilder.CreateTable(
                name: "ParkingLots",
                columns: table => new
                {
                    ParkId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParkArea = table.Column<long>(type: "bigint", nullable: false),
                    ParkName = table.Column<string>(type: "varchar(50)", nullable: true),
                    ParkPlace = table.Column<string>(type: "varchar(11)", nullable: true),
                    ParkPrice = table.Column<long>(type: "bigint", nullable: false),
                    ParkStatus = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParkingLots", x => x.ParkId);
                });

            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    TripId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookedTickedNumber = table.Column<int>(type: "int", nullable: false),
                    CarType = table.Column<string>(type: "varchar(11)", nullable: true),
                    DepartureDate = table.Column<DateTime>(type: "date", nullable: false),
                    DepartureTime = table.Column<TimeSpan>(type: "time", nullable: false),
                    Destination = table.Column<string>(type: "varchar(50)", nullable: true),
                    Driver = table.Column<string>(type: "varchar(50)", nullable: true),
                    MaximumOnlineNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.TripId);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                columns: table => new
                {
                    LicensePlate = table.Column<string>(type: "varchar(50)", nullable: false),
                    CarColor = table.Column<string>(type: "varchar(11)", nullable: true),
                    CarType = table.Column<string>(type: "varchar(50)", nullable: true),
                    Company = table.Column<string>(type: "varchar(50)", nullable: true),
                    ParkId = table.Column<long>(type: "bigint", nullable: false),
                    ParkingLotParkId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.LicensePlate);
                    table.ForeignKey(
                        name: "FK_Cars_ParkingLots_ParkingLotParkId",
                        column: x => x.ParkingLotParkId,
                        principalTable: "ParkingLots",
                        principalColumn: "ParkId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BookingOffices",
                columns: table => new
                {
                    OfficeId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EndContractDeadline = table.Column<DateTime>(type: "date", nullable: false),
                    OfficeName = table.Column<string>(type: "varchar(50)", nullable: true),
                    OfficePhone = table.Column<string>(type: "varchar(11)", nullable: true),
                    OfficePlace = table.Column<string>(type: "varchar(50)", nullable: true),
                    OfficePrice = table.Column<long>(type: "bigint", nullable: false),
                    StartContractDeadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TripId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingOffices", x => x.OfficeId);
                    table.ForeignKey(
                        name: "FK_BookingOffices_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "TripId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tickets",
                columns: table => new
                {
                    TicketId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookingTime = table.Column<TimeSpan>(type: "time", nullable: false),
                    CustomerName = table.Column<string>(type: "varchar(11)", nullable: true),
                    LicensePlate = table.Column<string>(type: "varchar(11)", nullable: true),
                    TripId = table.Column<long>(type: "bigint", nullable: false),
                    CarLicensePlate = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tickets", x => x.TicketId);
                    table.ForeignKey(
                        name: "FK_Tickets_Cars_CarLicensePlate",
                        column: x => x.CarLicensePlate,
                        principalTable: "Cars",
                        principalColumn: "LicensePlate",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tickets_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "TripId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "LicensePlate", "CarColor", "CarType", "Company", "ParkId", "ParkingLotParkId" },
                values: new object[,]
                {
                    { "29B-28221", "Black", "HUYNDAI", "Cam Van", 1L, null },
                    { "36B-03745", "White", "HUYNDAI", "Hoang Long", 2L, null },
                    { "28B-1111", "Black", "Truong Hai", "Phuong Trang", 3L, null }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeId", "Account", "Department", "EmployeeAddress", "EmployeeBirthDate", "EmployeeEmail", "EmployeeName", "EmployeePhone", "Password", "Sex" },
                values: new object[,]
                {
                    { 1L, "VinhNV", "FSD.BGH", " Hai Chau, Da Nang", new DateTime(1994, 9, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "VinhNV@gmail.com", "Nguyen Van Vinh", "0123456852", "Vinh123", "1" },
                    { 2L, "LanNT", "FSD.BGH", "Tan Ky, Quang Nam", new DateTime(1994, 2, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "LanNT@gmail.com", "Nguyen Thi Lan", "0123454751", "Lan123", "0" },
                    { 3L, "ThanhPV", "FSD.BGH", "Phu Vang, Hue", new DateTime(1994, 1, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "ThanhPV@gmail.com", "Pham Van Thanh", "0123456453", "Thanh123", "1" }
                });

            migrationBuilder.InsertData(
                table: "ParkingLots",
                columns: new[] { "ParkId", "ParkArea", "ParkName", "ParkPlace", "ParkPrice", "ParkStatus" },
                values: new object[,]
                {
                    { 1L, 20L, "Bai so 1", "Khu Dong", 600000L, "Blank" },
                    { 2L, 50L, "Bai so 2", "Khu Tay", 600000L, "Blank" },
                    { 3L, 60L, "Bai so 3", "Khu Nam", 600000L, "Blank" }
                });

            migrationBuilder.InsertData(
                table: "Trips",
                columns: new[] { "TripId", "BookedTickedNumber", "CarType", "DepartureDate", "DepartureTime", "Destination", "Driver", "MaximumOnlineNumber" },
                values: new object[,]
                {
                    { 1L, 1235462257, "Giuong nam", new DateTime(2022, 4, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 30, 0, 0), "Nam Dinh", "Nguyen Cong Thanh", 1235467856 },
                    { 2L, 1235462258, "Giuong nam", new DateTime(2022, 4, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0), "Ha Giang", "Le Anh Tuan", 1235467857 }
                });

            migrationBuilder.InsertData(
                table: "BookingOffices",
                columns: new[] { "OfficeId", "EndContractDeadline", "OfficeName", "OfficePhone", "OfficePlace", "OfficePrice", "StartContractDeadline", "TripId" },
                values: new object[,]
                {
                    { 1L, new DateTime(2022, 4, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Office 1", "0236514578", "Pham Van Bach", 0L, new DateTime(2022, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L },
                    { 2L, new DateTime(2022, 4, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Office 2", "0236514578", "Duy Tan", 0L, new DateTime(2022, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L },
                    { 3L, new DateTime(2022, 4, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Office 3", "0236514578", "Hoa Lac", 0L, new DateTime(2022, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 2L }
                });

            migrationBuilder.InsertData(
                table: "Tickets",
                columns: new[] { "TicketId", "BookingTime", "CarLicensePlate", "CustomerName", "LicensePlate", "TripId" },
                values: new object[,]
                {
                    { 1L, new TimeSpan(0, 8, 30, 0, 0), null, "Giang", "29B-28221", 1L },
                    { 2L, new TimeSpan(0, 8, 30, 0, 0), null, "Huyen", "29B-28221", 1L },
                    { 3L, new TimeSpan(0, 9, 0, 0, 0), null, "Tu", "36B-03745", 2L },
                    { 4L, new TimeSpan(0, 9, 0, 0, 0), null, "Thao", "36B-03745", 2L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BookingOffices_TripId",
                table: "BookingOffices",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ParkingLotParkId",
                table: "Cars",
                column: "ParkingLotParkId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_CarLicensePlate",
                table: "Tickets",
                column: "CarLicensePlate");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_TripId",
                table: "Tickets",
                column: "TripId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookingOffices");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Tickets");

            migrationBuilder.DropTable(
                name: "Cars");

            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropTable(
                name: "ParkingLots");
        }
    }
}
