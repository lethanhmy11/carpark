﻿using CarPark.Core.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Core.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        IBookingOfficeRepository BookingOfficeRepository { get; }
        ICarRepository CarRepository { get; }
        IEmployeeRepository EmployeeRepository { get; }
        IParkingLotRepository ParkingLotRepository { get; }
        ITicketRepository TicketRepository { get; }
        ITripRepository TripRepository { get; }
        CarParkDBContext CarParkDBContext { get; }
        int SaveChange();
    }
}
