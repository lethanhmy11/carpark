﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace CarPark.Core.Infrastructures
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly CarParkDBContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(CarParkDBContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }


        /// <summary>
        /// Change state of entity to added.
        /// </summary>
        /// <param name="entity"></param>
        public void Create(TEntity entity)
        {
            _dbSet.Add(entity);

        }

        /// <summary>
        /// Change state of entities to added.
        /// </summary>
        /// <param name="entity"></param>
        public void CreateRange(IEnumerable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        /// <summary>
        /// Change state of entity to deleted.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void Delete(long id)
        {
            TEntity entity = _dbSet.Find(id);
            if (entity == null)
                throw new ArgumentNullException($"Khong the tim thay {entity.GetType().Name} Id={id}");
            _dbSet.Remove(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public IEnumerable<TEntity> Find(Func<TEntity, bool> confident)
        {
            return _dbSet.Where(confident);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public TEntity GetById(long primaryKey)
        {
            return _dbSet.Find(primaryKey);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            _dbSet.UpdateRange(entities);
        }
    }
}
