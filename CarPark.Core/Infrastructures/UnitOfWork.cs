﻿using CarPark.Core.IRepositories;
using CarPark.Core.Repositories;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Core.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private IBookingOfficeRepository _bookingOfficeRepository;
        private ICarRepository _carRepository;
        private IEmployeeRepository _employeeRepository;
        private IParkingLotRepository _parkingLotRepository;
        private ITicketRepository _ticketRepository;
        private ITripRepository _tripRepository;
        private readonly CarParkDBContext _context;
        private readonly IOptions<AppSettings> _appsettings;

        public UnitOfWork(CarParkDBContext context, IOptions<AppSettings> appsettings)
        {
            _context = context;
            _appsettings = appsettings;
        }

        public IBookingOfficeRepository BookingOfficeRepository => _bookingOfficeRepository ?? (_bookingOfficeRepository = new BookingOfficeRepository(_context));

        public ICarRepository CarRepository => _carRepository ?? (_carRepository = new CarRepository(_context));

        public IEmployeeRepository EmployeeRepository => _employeeRepository ?? (_employeeRepository = new EmployeeRepository(_context, _appsettings));

        public IParkingLotRepository ParkingLotRepository => _parkingLotRepository ?? (_parkingLotRepository = new ParkingLotRepository(_context));

        public ITicketRepository TicketRepository => _ticketRepository ?? (_ticketRepository = new TicketRepository(_context));

        public ITripRepository TripRepository => _tripRepository ?? (_tripRepository = new TripRepository(_context));

        public CarParkDBContext CarParkDBContext => _context;

        public void Dispose()
        {
            _context.Dispose();
        }

        public int SaveChange()
        {
            return _context.SaveChanges();
        }
    }
}
