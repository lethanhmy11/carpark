﻿using System;
using System.Collections.Generic;

namespace CarPark.Core.Infrastructures
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        void Create(TEntity entity);
        void CreateRange(IEnumerable<TEntity> entities);
        void Delete(TEntity entity);
        void Delete(long id);
        void DeleteRange(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(long id);
        IEnumerable<TEntity> Find(Func<TEntity, bool> confident);
    }
}
