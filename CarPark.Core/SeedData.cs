﻿using CarPark.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Core
{
    public static class SeedData
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().HasData(
                new Employee() { EmployeeId = 1, EmployeeName = "Nguyen Van Vinh", Account="VinhNV", Department="FSD.BGH", EmployeeBirthDate= new DateTime(1994, 09, 08), Sex="1", EmployeeAddress=" Hai Chau, Da Nang", EmployeeEmail="VinhNV@gmail.com", EmployeePhone="0123456852", Password="Vinh123", Role="HRM"},
                new Employee() { EmployeeId = 2, EmployeeName = "Nguyen Thi Lan", Account = "LanNT", Department = "FSD.BGH", EmployeeBirthDate = new DateTime(1994, 02, 08), Sex = "0", EmployeeAddress = "Tan Ky, Quang Nam", EmployeeEmail = "LanNT@gmail.com", EmployeePhone = "0123454751", Password = "Lan123" , Role="Admin"},
                new Employee() { EmployeeId = 3, EmployeeName = "Pham Van Thanh", Account = "ThanhPV", Department = "FSD.BGH", EmployeeBirthDate = new DateTime(1994, 01, 08), Sex = "1", EmployeeAddress = "Phu Vang, Hue", EmployeeEmail = "ThanhPV@gmail.com", EmployeePhone = "0123456453", Password = "Thanh123", Role = "Admin" }
            );
            modelBuilder.Entity<ParkingLot>().HasData(
                new ParkingLot() {ParkId = 1, ParkArea= 20, ParkName = "Bai so 1", ParkPlace="Khu Dong", ParkPrice=600000, ParkStatus = "Blank"},
                new ParkingLot() { ParkId = 2, ParkArea = 50, ParkName = "Bai so 2", ParkPlace = "Khu Tay", ParkPrice = 600000, ParkStatus = "Blank" },
                new ParkingLot() { ParkId = 3, ParkArea = 60, ParkName = "Bai so 3", ParkPlace = "Khu Nam", ParkPrice = 600000, ParkStatus = "Blank" }
            );
            modelBuilder.Entity<Trip>().HasData(
                new Trip() { TripId = 1, BookedTickedNumber= 1235462257, CarType = "Giuong nam", DepartureDate =  new DateTime(2022,04, 20), DepartureTime = new TimeSpan(8, 30, 00), Destination="Nam Dinh", Driver="Nguyen Cong Thanh", MaximumOnlineNumber=01235467856},
                new Trip() { TripId = 2, BookedTickedNumber = 1235462258, CarType = "Giuong nam", DepartureDate = new DateTime(2022, 04, 21), DepartureTime = new TimeSpan(8, 00, 00), Destination = "Ha Giang", Driver = "Le Anh Tuan", MaximumOnlineNumber = 01235467857 }
            );

            modelBuilder.Entity<Car>().HasData(
                new Car() { LicensePlate = "29B-28221", CarType = "HUYNDAI", CarColor = "Black", Company="Cam Van", ParkId = 1},
                new Car() { LicensePlate = "36B-03745", CarType = "HUYNDAI", CarColor = "White", Company = "Hoang Long", ParkId = 2 },
                new Car() { LicensePlate = "28B-1111", CarType = "Truong Hai", CarColor = "Black", Company = "Phuong Trang", ParkId = 3 }
            );

            modelBuilder.Entity<Ticket>().HasData(
                new Ticket() { TicketId = 1, BookingTime = new TimeSpan(8, 30, 00), CustomerName = "Giang", LicensePlate = "29B-28221", TripId = 1 },
                new Ticket() { TicketId = 2, BookingTime = new TimeSpan(8, 30, 00), CustomerName = "Huyen", LicensePlate = "29B-28221", TripId = 1 },
                new Ticket() { TicketId = 3, BookingTime = new TimeSpan(9, 00, 00), CustomerName = "Tu", LicensePlate = "36B-03745", TripId = 2 },
                new Ticket() { TicketId = 4, BookingTime = new TimeSpan(9, 00, 00), CustomerName = "Thao", LicensePlate = "36B-03745", TripId = 2 }
            );

            modelBuilder.Entity<BookingOffice>().HasData(
                new BookingOffice() { OfficeId = 1, EndContractDeadline= new DateTime(2022, 04, 30), OfficeName="Office 1", OfficePhone="0236514578", OfficePlace="Pham Van Bach", StartContractDeadline= new DateTime(2022, 04, 20), TripId=1 },
                new BookingOffice() { OfficeId = 2, EndContractDeadline = new DateTime(2022, 04, 30), OfficeName = "Office 2", OfficePhone = "0236514578", OfficePlace = "Duy Tan", StartContractDeadline = new DateTime(2022, 04, 20), TripId = 1 },
                new BookingOffice() { OfficeId = 3, EndContractDeadline = new DateTime(2022, 04, 30), OfficeName = "Office 3", OfficePhone = "0236514777", OfficePlace = "Hoa Lac", StartContractDeadline = new DateTime(2022, 04, 20), TripId = 2 }
            );
        }
    }
}
