﻿using CarPark.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Core
{
    public class CarParkDBContext : DbContext
    {
        public CarParkDBContext()
        {

        }

        public CarParkDBContext(DbContextOptions<CarParkDBContext> options) : base(options)
        {
        }

        public DbSet<BookingOffice> BookingOffices { get; set; }

        public DbSet<Car> Cars { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<ParkingLot> ParkingLots { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<Trip> Trips { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                string connectionString = "Server=.;Database=CarPark;Trusted_Connection=True; MultipleActiveResultSets = true";
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BookingOffice>(sp =>
            {
                sp.Property(c => c.OfficeId)
                    .HasColumnType("bigint");

                sp.Property(c => c.EndContractDeadline)
                    .HasColumnType("date");

                sp.Property(c => c.OfficeName)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.OfficePhone)
                    .HasColumnType("varchar(11)");

                sp.Property(c => c.OfficePlace)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.OfficePrice)
                    .HasColumnType("bigint");

                sp.Property(c => c.TripId)
                    .HasColumnType("bigint");
            });

            modelBuilder.Entity<Trip>(sp =>
            {
                sp.Property(c => c.TripId)
                    .HasColumnType("bigint");

                sp.Property(c => c.BookedTickedNumber)
                    .HasColumnType("int");

                sp.Property(c => c.CarType)
                    .HasColumnType("varchar(11)");

                sp.Property(c => c.DepartureDate)
                    .HasColumnType("date");

                sp.Property(c => c.DepartureTime)
                    .HasColumnType("time");

                sp.Property(c => c.Destination)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.Driver)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.MaximumOnlineNumber)
                    .HasColumnType("int");
            });

            modelBuilder.Entity<Ticket>(sp =>
            {
                sp.Property(c => c.TicketId)
                    .HasColumnType("bigint");

                sp.Property(c => c.BookingTime)
                    .HasColumnType("time");

                sp.Property(c => c.CustomerName)
                    .HasColumnType("varchar(11)");

                sp.Property(c => c.LicensePlate)
                    .HasColumnType("varchar(11)");

                sp.Property(c => c.TripId)
                    .HasColumnType("bigint");
            });

            modelBuilder.Entity<Car>(sp =>
            {
                sp.Property(c => c.LicensePlate)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.CarColor)
                    .HasColumnType("varchar(11)");

                sp.Property(c => c.CarType)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.Company)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.ParkId)
                    .HasColumnType("bigint");
            });

            modelBuilder.Entity<ParkingLot>(sp =>
            {
                sp.Property(c => c.ParkId)
                    .HasColumnType("bigint");

                sp.Property(c => c.ParkArea)
                    .HasColumnType("bigint");

                sp.Property(c => c.ParkName)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.ParkPlace)
                    .HasColumnType("varchar(11)");

                sp.Property(c => c.ParkPrice)
                    .HasColumnType("bigint");

                sp.Property(c => c.ParkStatus)
                    .HasColumnType("varchar(50)"); 
            });

            modelBuilder.Entity<Employee>(sp =>
            {

                sp.Property(c => c.EmployeeId)
                    .HasColumnType("bigint");

                sp.Property(c => c.Account)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.Department)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.EmployeeAddress)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.EmployeeBirthDate)
                    .HasColumnType("date");

                sp.Property(c => c.EmployeeEmail)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.EmployeeName)
                    .HasColumnType("varchar(50)");

                sp.Property(c => c.EmployeePhone)
                    .HasColumnType("varchar(10)");

                sp.Property(c => c.Password)
                    .HasColumnType("varchar(20)");

                sp.Property(c => c.Sex)
                    .HasColumnType("varchar(1)");

            });

            modelBuilder.Seed();
        }
    }
}
