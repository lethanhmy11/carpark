﻿using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using System;
using System.Collections.Generic;

namespace CarPark.Core.IRepositories
{
    public interface ITripRepository : IGenericRepository<Trip>
    {
        IEnumerable<Trip> Filter(DateTime departureDate);
    }
}
