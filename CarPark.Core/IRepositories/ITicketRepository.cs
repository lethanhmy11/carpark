﻿using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using System;
using System.Collections.Generic;

namespace CarPark.Core.IRepositories
{
    public interface ITicketRepository : IGenericRepository<Ticket>
    {
        Ticket GetDetailById(long id);
        IEnumerable<Ticket> GetDetailAll();
        IEnumerable<Ticket> Filter(string licensePlate, string destination, string customer, DateTime departureDate);
    }
}
