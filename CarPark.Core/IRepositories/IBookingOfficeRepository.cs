﻿using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using System.Collections.Generic;

namespace CarPark.Core.IRepositories
{
    public interface IBookingOfficeRepository : IGenericRepository<BookingOffice>
    {
        BookingOffice GetDetailById(long id);
        IEnumerable<BookingOffice> GetDetailAll();
        IEnumerable<BookingOffice> Filter(string name, string phone, string place);
    }
}
