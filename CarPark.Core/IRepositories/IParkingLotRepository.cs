﻿using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using System.Collections.Generic;

namespace CarPark.Core.IRepositories
{
    public interface IParkingLotRepository : IGenericRepository<ParkingLot>
    {
        IEnumerable<ParkingLot> Filter(string name, string place);
    }
}
