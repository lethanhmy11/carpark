﻿using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using System.Collections.Generic;

namespace CarPark.Core.IRepositories
{
    public interface IEmployeeRepository : IGenericRepository<Employee>
    {
        IEnumerable<Employee> Filter(string name, string department, string sex);
        Employee Authenticate(string account, string password);
    }
}
