﻿using CarPark.Core.Infrastructures;
using CarPark.Core.Models;
using System.Collections.Generic;

namespace CarPark.Core.IRepositories
{
    public interface ICarRepository : IGenericRepository<Car>
    {
        Car GetDetailById(string licensePlate);
        IEnumerable<Car> GetDetailAll();
        IEnumerable<Car> Filter(string licensePlate, string carType, string company);
        void Delete(string licensePlate);
    }
}
