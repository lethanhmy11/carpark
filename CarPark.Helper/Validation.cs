﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.Helper
{
    public static class Validation
    {
        public static bool PasswordValidation(this string password)
        {
            char[] array = password.ToCharArray();
            int lower = 0, upper = 0, digits = 0;

            if (password.Length < 6)
                return false;

            for (int i = 0; i < array.Length; i++)
            {
                if (Char.IsDigit(array[i]))
                    digits++;
                if (Char.IsUpper(array[i]))
                    lower++;
                if (Char.IsLower(array[i]))
                    upper++;
            }

            if (lower == 0)
                return false;

            if (upper == 0)
                return false;

            if (digits == 0)
                return false;

            return true;
        }
    }
}
