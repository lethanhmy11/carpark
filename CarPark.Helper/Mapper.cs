﻿using AutoMapper;
using CarPark.Core.Models;
using CarPark.ViewModels.BookingOffices;
using CarPark.ViewModels.Cars;
using CarPark.ViewModels.Employees;
using CarPark.ViewModels.ParkingLots;
using CarPark.ViewModels.Tickets;
using CarPark.ViewModels.Trips;

namespace CarPark.Helper
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<Employee, EmployeeViewModel>().ReverseMap();

            CreateMap<Trip, TripViewModel>().ReverseMap();
            CreateMap<Trip, CreateTripViewModel>().ReverseMap();
            CreateMap<Trip, TripDto>().ReverseMap();

            CreateMap<BookingOffice, BookingOfficeViewModel>().ReverseMap();
            CreateMap<BookingOffice, BookingOfficeDetailViewModel>().ReverseMap();
            CreateMap<BookingOffice, CreateBookingOfficeViewModel>().ReverseMap();

            CreateMap<Car, CarViewModel>().ReverseMap();
            CreateMap<Car, CreateCarViewModel>().ReverseMap();

            CreateMap<ParkingLot, ParkingLotViewModel>().ReverseMap();
            CreateMap<ParkingLot, CreateParkingLotViewModel>().ReverseMap();

            CreateMap<Ticket, TicketViewModel>().ReverseMap();
            CreateMap<Ticket, CreateTicketViewModel>().ReverseMap();
        }
    }
}
