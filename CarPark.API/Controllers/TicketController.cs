﻿using CarPark.Services.IServices;
using CarPark.ViewModels.Tickets;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CarPark.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketServices _ticketServices;

        public TicketController(ITicketServices ticketServices)
        {
            _ticketServices = ticketServices;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(long id)
        {
            var ticket = _ticketServices.GetById(id);
            return Ok(ticket);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var tickets = _ticketServices.GetAll();
            return Ok(tickets);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(string lincensePlate, string destination, string customer, DateTime departureDate)
        {
            var tickets = _ticketServices.Filter(lincensePlate, destination, customer, departureDate);
            return Ok(tickets);
        }

        [HttpPost]
        public IActionResult Create(CreateTicketViewModel ticket)
        {
            if (ticket == null)
                return BadRequest();
            var response = _ticketServices.Create(ticket);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Edit(CreateTicketViewModel ticket)
        {
            if (ticket == null)
                return BadRequest();
            var response = _ticketServices.Edit(ticket);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpDelete]
        [Route("{ticketId}")]
        public IActionResult Delete(long ticketId)
        {
            if (ticketId == null)
                return BadRequest();
            var response = _ticketServices.Delete(ticketId);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return NoContent();
        }
    }
}
