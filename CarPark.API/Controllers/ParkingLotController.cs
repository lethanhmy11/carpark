﻿using CarPark.Services.IServices;
using CarPark.ViewModels.ParkingLots;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarPark.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    public class ParkingLotController : ControllerBase
    {
        private readonly IParkingLotServices _parkingLotServices;

        public ParkingLotController(IParkingLotServices parkingLotServices)
        {
            _parkingLotServices = parkingLotServices;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(long id)
        {
            var parkingLot = _parkingLotServices.GetById(id);
            return Ok(parkingLot);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var parkingLots = _parkingLotServices.GetAll();
            return Ok(parkingLots);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(string name, string place)
        {
            var parkingLots = _parkingLotServices.Filter(name, place);
            return Ok(parkingLots);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(CreateParkingLotViewModel parkingLot)
        {
            if (parkingLot == null)
                return BadRequest();
            var response = _parkingLotServices.Create(parkingLot);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Edit(CreateParkingLotViewModel parkingLot)
        {
            if (parkingLot == null)
                return BadRequest();
            var response = _parkingLotServices.Edit(parkingLot);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpDelete]
        [Route("{parkingLotId}")]
        public IActionResult Delete(long parkingLotId)
        {
            if (parkingLotId == null)
                return BadRequest();
            var response = _parkingLotServices.Delete(parkingLotId);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return NoContent();
        }
    }
}
