﻿using CarPark.Core.Models;
using CarPark.Services.IServices;
using CarPark.ViewModels.BookingOffices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarPark.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles="Admin")]
    [ApiController]
    public class BookingOfficeController : ControllerBase
    {
        private readonly IBookingOfficeServices _bookingOfficeServices;

        public BookingOfficeController(IBookingOfficeServices bookingOfficeServices)
        {
            _bookingOfficeServices = bookingOfficeServices;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(long id)
        {
            var bookingOffice = _bookingOfficeServices.GetById(id);
            return Ok(bookingOffice);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var bookingOffices = _bookingOfficeServices.GetAll();
            return Ok(bookingOffices);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(string name, string phone, string place)
        { 
            var bookingOffices = _bookingOfficeServices.Filter(name, phone, place);
            return Ok(bookingOffices);
        }

        [HttpPost]
        public IActionResult Create(CreateBookingOfficeViewModel bookingOffice)
        {
            if (bookingOffice == null)
                return BadRequest();
            var response = _bookingOfficeServices.Create(bookingOffice);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Edit(CreateBookingOfficeViewModel bookingOffice)
        {
            if (bookingOffice == null)
                return BadRequest();
            var response = _bookingOfficeServices.Edit(bookingOffice);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpDelete]
        [Route("{bookingOfficeId}")]
        public IActionResult Delete(long bookingOfficeId)
        {
            if (bookingOfficeId == null)
                return BadRequest();
            var response = _bookingOfficeServices.Delete(bookingOfficeId);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return NoContent();
        }
    }
}
