﻿using CarPark.Services.IServices;
using CarPark.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarPark.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IEmployeeServices _employeeServices;

        public AccountController(IEmployeeServices employeeServices)
        {
            _employeeServices = employeeServices;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public IActionResult Login([FromBody] Login model)
        {
            var employee = _employeeServices.Authenticate(model.Account, model.Password);
            if (employee == null)
            {
                return BadRequest(new { message = "Account or password is incorrect" });
            }
            return Ok(employee);
        }
    }
}
