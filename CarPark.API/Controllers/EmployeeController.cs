﻿using CarPark.Core.Models;
using CarPark.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarPark.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "HRM")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeServices _employeeServices;

        public EmployeeController(IEmployeeServices employeeServices)
        {
            _employeeServices = employeeServices;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(long id)
        {
            var employee = _employeeServices.GetById(id);
            return Ok(employee);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var employees = _employeeServices.GetAll();
            return Ok(employees);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(string name, string departname, string sex)
        {
            var employees = _employeeServices.Filter(name, departname, sex);
            return Ok(employees);
        }

        [HttpPost]
        public IActionResult Create(Employee employee)
        {
            if (employee == null)
                return BadRequest();
            var response = _employeeServices.Create(employee);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Edit(Employee employee)
        {
            if (employee == null)
                return BadRequest();
            var response = _employeeServices.Edit(employee);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpDelete]
        [Route("{employeeId}")]
        public IActionResult Delete(long employeeId)
        {
            if (employeeId == null)
                return BadRequest();
            var response = _employeeServices.Delete(employeeId);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return NoContent();
        }
    }
}
