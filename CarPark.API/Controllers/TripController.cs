﻿using CarPark.Services.IServices;
using CarPark.ViewModels.Trips;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CarPark.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    public class TripController : ControllerBase
    {
        private readonly ITripServices _tripServices;

        public TripController(ITripServices tripServices)
        {
            _tripServices = tripServices;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var trip = _tripServices.GetById(id);
            return Ok(trip);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var trips = _tripServices.GetAll();
            return Ok(trips);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(DateTime departureDate)
        {
            var trips = _tripServices.Filter(departureDate);
            return Ok(trips);
        }

        [HttpPost]
        public IActionResult Create(CreateTripViewModel trip)
        {
            if (trip == null)
                return BadRequest();
            var response = _tripServices.Create(trip);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Edit(CreateTripViewModel trip)
        {
            if (trip == null)
                return BadRequest();
            var response = _tripServices.Edit(trip);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpDelete]
        [Route("{tripId}")]
        public IActionResult Delete(long tripId)
        {
            if (tripId == null)
                return BadRequest();
            var response = _tripServices.Delete(tripId);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return NoContent();
        }
    }
}