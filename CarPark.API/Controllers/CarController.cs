﻿using CarPark.Core.Models;
using CarPark.Services.IServices;
using CarPark.ViewModels.Cars;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarPark.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private readonly ICarServices _carServices;

        public CarController(ICarServices carServices)
        {
            _carServices = carServices;
        }

        [HttpGet]
        [Route("{lincensePlate}")]
        public IActionResult GetById(string lincensePlate)
        {
            var car = _carServices.GetById(lincensePlate);
            return Ok(car);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var cars = _carServices.GetAll();
            return Ok(cars);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(string lincensePlate, string carType, string company)
        {
            var cars = _carServices.Filter(lincensePlate, carType, company);
            return Ok(cars);
        }

        [HttpPost]
        public IActionResult Create(CreateCarViewModel car)
        {
            if (car == null)
                return BadRequest();
            var response = _carServices.Create(car);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Edit(CreateCarViewModel car)
        {
            if (car == null)
                return BadRequest();
            var response = _carServices.Edit(car);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return Ok();
        }

        [HttpDelete]
        [Route("{lincensePlate}")]
        public IActionResult Delete(string lincensePlate)
        {
            if (lincensePlate == null)
                return BadRequest();
            var response = _carServices.Delete(lincensePlate);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new { response.ErrrorMessages });
            }
            return NoContent();
        }
    }
}
