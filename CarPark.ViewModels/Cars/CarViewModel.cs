﻿using CarPark.ViewModels.ParkingLots;

namespace CarPark.ViewModels.Cars
{
    public class CarViewModel
    {
        public string LicensePlate { get; set; }

        public string CarColor { get; set; }

        public string CarType { get; set; }

        public string Company { get; set; }

        public long ParkId { get; set; }

        public ParkingLotViewModel ParkingLot { get; set; }

    }
}
