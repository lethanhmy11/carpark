﻿using CarPark.ViewModels.Trips;

namespace CarPark.ViewModels.BookingOffices
{
    public class BookingOfficeViewModel
    {
        public long OfficeId { get; set; }

        public string OfficeName { get; set; }

        public TripDto Trip { get; set; }
    }
}
