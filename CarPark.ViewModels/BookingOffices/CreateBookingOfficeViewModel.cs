﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.ViewModels.BookingOffices
{
    public class CreateBookingOfficeViewModel
    {
        public long OfficeId { get; set; }

        public DateTime EndContractDeadline { get; set; }

        public string OfficeName { get; set; }

        public string OfficePhone { get; set; }

        public string OfficePlace { get; set; }

        public long OfficePrice { get; set; }

        public DateTime StartContractDeadline { get; set; }

        public long TripId { get; set; }

    }
}
