﻿using CarPark.ViewModels.Trips;
using System;

namespace CarPark.ViewModels.BookingOffices
{
    public class BookingOfficeDetailViewModel
    {
        public long OfficeId { get; set; }

        public DateTime EndContractDeadline { get; set; }

        public string OfficeName { get; set; }

        public string OfficePhone { get; set; }

        public string OfficePlace { get; set; }

        public long OfficePrice { get; set; }

        public DateTime StartContractDeadline { get; set; }

        public long TripId { get; set; }

        public TripViewModel Trip { get; set; }
    }
}
