﻿namespace CarPark.ViewModels.ParkingLots
{
    public class CreateParkingLotViewModel
    {
        public long ParkId { get; set; }

        public long ParkArea { get; set; }

        public string ParkName { get; set; }

        public string ParkPlace { get; set; }

        public long ParkPrice { get; set; }

        public string ParkStatus { get; set; }
    }
}
