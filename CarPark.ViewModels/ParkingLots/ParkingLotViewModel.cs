﻿namespace CarPark.ViewModels.ParkingLots
{
    public class ParkingLotViewModel
    {
        public long ParkId { get; set; }

        public string ParkName { get; set; }

    }
}
