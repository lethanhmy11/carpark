﻿

using System;

namespace CarPark.ViewModels.Trips
{
    public class TripViewModel
    {
        public long TripId { get; set; }

        public long BookedTickedNumber { get; set; }

        public string CarType { get; set; }

        public DateTime DepartureDate { get; set; }

        public TimeSpan DepartureTime { get; set; }

        public string Destination { get; set; }

        public string Driver { get; set; }

    }
}
