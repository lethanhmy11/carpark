﻿namespace CarPark.ViewModels.Trips
{
    public class TripDto
    {
        public long TripId { get; set; }

        public string Destination { get; set; }
    }
}
