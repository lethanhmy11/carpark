﻿using System;

namespace CarPark.ViewModels.Tickets
{
    public class CreateTicketViewModel
    {
        public long TicketId { get; set; }

        public TimeSpan BookingTime { get; set; }

        public string CustomerName { get; set; }

        public string LicensePlate { get; set; }

        public long TripId { get; set; }
    }
}
