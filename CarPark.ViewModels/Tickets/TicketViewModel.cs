﻿using CarPark.ViewModels.Trips;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPark.ViewModels.Tickets
{
    public class TicketViewModel
    {
        public long TicketId { get; set; }

        public TimeSpan BookingTime { get; set; }

        public string CustomerName { get; set; }

        public string LicensePlate { get; set; }

        public long TripId { get; set; }

        public TripDto Trip { set; get; }
    }
}
