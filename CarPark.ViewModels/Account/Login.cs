﻿using System.ComponentModel.DataAnnotations;


namespace CarPark.ViewModels.Account
{
    public class Login
    {
        [Required]
        public string Account { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
