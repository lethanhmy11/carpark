﻿using System;


namespace CarPark.ViewModels.Employees
{
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }

        public string Department { get; set; }

        public string EmployeeAddress { get; set; }

        public DateTime EmployeeBirthDate { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeePhone { get; set; }

    }
}
